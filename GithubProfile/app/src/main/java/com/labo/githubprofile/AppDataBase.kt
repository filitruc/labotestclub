package com.labo.githubprofile

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = arrayOf(User::class), version = 1)
abstract class AppDataBase : RoomDatabase(){
    companion object {
        private var appDataBase: AppDataBase? = null

        @JvmStatic fun getDbInstance(context: Context): AppDataBase {
            if (appDataBase == null)
                appDataBase = Room.databaseBuilder(context, AppDataBase::class.java, "database-name").build()

            return appDataBase!!
        }
    }
    abstract fun userDao(): UserDao


}