package com.todos.todos;

import java.util.Date;

/**
 * Created by Filipe on 2018-01-23.
 */

public class Task {
    private String name;
    private Date dueDate;
    private Boolean completed;

    public Task(String name, boolean completed, Date dueDate) {
        this.name = name;
        this.completed = completed;
        this.dueDate = dueDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public String getName() {
        return name;
    }

    public boolean isCompleted() {
        return completed;
    }
}
